# Roshar Repo

Proyecto de una web informativa detallando al planeta Roshar dentro de la saga de libro "stormlight archive" de Brandon Sanderson 

# Plataforma Plus

[![](https://img.shields.io/badge/.NET%20Core-5.0-blue)](https://dotnet.microsoft.com/download/dotnet/5.0)

## Index
* Instalaciones
    * [Microsoft Visual Studio 2019](#MVS) 
    * [GIT](#GIT)
* Configuraciones
    * [Configuracion de tu GIT](#Config)
    * [Generar llave SSH](#SSHKey)
    * [Como clonar el repositorio](#Repo)
* flujo de trabajo en GIT
    * [Nombrar Ramas](#Naming)
    * [Crear Ramas](#Creating)
    * [Merge Request](#MR)

<a name="MVS"><a/>
## Microsoft Visual Studio 2019
Puedes descargar el IDE Microsoft Visual Studio 2019 [aqui](https://visualstudio.microsoft.com/es/downloads/). 

<a name="GIT"><a/>
## GIT
Puedes descargar GIT [Aqui](https://git-scm.com/downloads).

<a name="Config"><a/>
## Configura tu git
Ejecuta los siguientes commandos en tu git bash:
```
git config --global user.name "[Name]"
git config --global user.email "[Your account e-mail]"
```
<a name="SSHKey"><a/>
## Generar llave SSH
* Abre la Git Bash y ejecuta el siguiente comando:
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com"
```
* Cuando se te indique "Ingresar un archivo donde guardar la clave", presiona Intro. Al hacerlo aceptas la ubicación predeterminada del archivo.
* A continuacion te pedirá registrar una contraseña.

Para más información sobre llaves SSH [Aqui](https://docs.github.com/es/github/authenticating-to-github/connecting-to-github-with-ssh)

<a name="Repo"><a/>
## Como clonar el repositorio
* En la parte superior del repositorio en Git Hub aparece un botón para clonarlo por SSH o por HTTPS
* Copia la Ruta de cualquiera de las dos formas.
* Crea una carpeta en donde almacenarás el repositorio.
* Abre la consola de comandos o el git bash con la ruta en la carpeta que creaste.
*  Ejecuta los siguientes comandos:
```
git init
git clone [Ruta HTTPS/ SSH]
```
<a name="Naming"><a/>
## Naming branches
Aquí hay muchos tipos de tareas en un proyecto. Cada tarea tiene un prefijo que usaremos para nombrar las ramas.
La siguiente tabla nos muestra los tipos de tareas, su prefijo y un ejemplo de nombre de rama.

|   Task  | Prefix |  Example  |                              Obvservation                             |
|:-------:|:------:|:---------:|:---------------------------------------------------------------------:|
| Develop |   DEV  |  DEV-235  | This task is for any kind of develop for the app.                     |
|   Bug   |   BUG  |   BUG-32  | This task is only used when a bug will be fixed.                      |
|  Hotfix | HOTFIX | HOTFIX-99 | This task is only used when a critical bug will be fixed immediately. |

<a name="Creating"><a/>
## Creating branches
All the branches will be created from the **develop branch**.
Follow the next steps:
1.  Create your branch in GitLab from the **develop branch** and name it according to the task prefix and the number of the task.
2.  In your **Git bash** go to the **develop branch** and execute `git pull` to pull the latest changes (and the branches created).
3.  Checkout to your new branch and GO TO WORK, SLAVE!

<a name="MR"><a/>
## Merge Request
A Merge Request is a petition to merge your changes to the **develop branch**. However, that changes have to be accepted for a Mainteiner.
The Mainteiner can reject your changes if consider that it can improve.
Follow the next steps:
1.  In your File Explorer go to *apollo-web-react folder*
2.  Right click an select Git GUI Here
* **Staged changes Area**
    * Here appear the files that you will commit.
* **Unstaged changes Area**
    * Here appear the files that you created, deleted and modified.
    * You can add files to **staged changes area** doing click on the file's icon.
3.  Add all the files that you want to commit.
4.  In **Commit message Area** write a short description of what did you do.
5.  Click on **Commit button*.
6.  Click on **Push button**.
7.  Select **your branch**.
8.  And finally click on **Push button**.

Now, you have to create the **Merge Request** in GitLab.
1.  Add the title: [Branch name] | [Short description] (e.g. DEV-37 | Login component and new icon)
2.  Add a description explaining briefly what did you do.
3.  Select a Mainteiner wich will accept o reject your MR.
4.  And finally create it.

